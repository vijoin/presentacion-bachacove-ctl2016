## Presentación ODP para las ponencias de la Comunidad BachacoVE en el Congreso
de Tecnologías Libres 2016

### Trabajando en este documento:
* Victor Inojosa

### Para convertir el archivo a ODP se usará ODPDOWN:
* http://libreoffice.org.ar/2015/03/fue-anunciado-odpdown-un-conversor-de-markdown-a-odp/
* https://github.com/thorstenb/odpdown
* https://people.freedesktop.org/~thorsten/odpdown-samples/demo-basics.pdf

odpdown presentacion.md plantilla-NO-MODIFICAR-NI-USAR.odp presentacion.odp