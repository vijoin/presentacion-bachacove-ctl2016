Comunidad BachacoVE y Odoo como propuesta para el ERP Nacional
======================================

## Contenido

* BachacoVE
* Proyecto del Ecosistema Productivo + PRO Nacional
* Odoo

## BachacoVE
Es una comunidad de alcance nacional que agrupa activistas, estudiantes,
docentes, entusiastas, emprendedores, trabajadores de instituciones públicas
y privadas interesados en el desarrollo e implantación de sistemas PRO
completamente basados en Tecnologías Libres

##BachacoVE - Objetivos
* Desarrollar el PRO Nacional
* Construir un Ecosistema Comunitario Productivo
* para el PRO Nacional
* Promover el Desarrollo Colaborativo entre las
* Instituciones Públicas
* Masificar el uso del PRO Nacional, tanto en el
* Poder Público como en el Poder Popular
* Masificar las capacidades de desarrollo de módulos en las Universidades

##BachacoVE - Estructura Organizativa
* Organización
* Formación
* Producción

## BachacoVE - Contactos
* @BachacoVE
* www.bachaco.org.ve
* Lista riseup.net
* Grupo Telegram (Activos en la Estructura)
* https://github.com/Bachaco-ve

## Proyecto del Ecosistema Productivo + PRO Nacional

1. Desarrollar un Ecosistema de Unidades Productivas con capacidades tecnopolíticas y principios colaborativos para el desarrollo de programas informáticos para la gestión del Estado, el Poder Popular y el sector privado.
2. Desarrollar un sistema formativo que incentive la participación activa de los sujetos en formación con la finalidad de incorporarse al ecosistema productivo, incidiendo en el sistema universitario actual y las plataformas de formación especializada.
3. Desarrollar una plataforma informática integral e integrada (PRO) para la gestión pública que coadyuve en la transparencia y la simplificación de trámites, así como la interoperabilidad entre instituciones.

## Módulos del Núcleo Administrativo-Financiero

1. Planificación
2. Presupuesto
3. Tesorería
4. Contabilidad
5. Inventario/Almacén
6. Bienes
7. Talento Humano
8. Georeferenciación
9. Inteligencia de Negocios

## Odoo
![Logo Odoo](imagenes/Odoo_Official_Logo.png)

(odoo.com)

## Odoo - Arquitectura
![Arquitectura Odoo](imagenes/odoo-architecture.jpg)

(https://www.odoo.com/documentation/)

## Odoo - Apps Disponibles
![Modulos Odoo](imagenes/modulos-odoo.png)

(https://demo2.odoo.com/web)

## Odoo - RAD (Desarrollo Rápido de Aplicaciones)
UML + Dia --> Codegen.py = Módulo Odoo
(https://github.com/vijoin/codegen-odoo8)

## Odoo - Entorno de Desarrollo
Debian Jessie + Docker + Odoo
(https://gist.github.com/vijoin/3ccb0df8997babb37603)
(https://hub.docker.com/_/odoo/)

## Odoo - Integración Contínua (Runbot)
![Modulos Odoo](imagenes/runbot.png)

(http://runbot.odoo.com/runbot)
(https://github.com/OCA/runbot-addons)

## MUCHAS GRACIAS

Contacto:
email: vijoin@riseup.net
twitter: @vijoin
Git(Hub/Lab): vijoin

Código Fuente de esta presentación:
* https://gitlab.com/vijoin/presentacion-bachacove-ctl2016.git